package com.example.openapidsl

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class JSONDslTest {

    @Test
    fun `add empty array to json`() {
        val json = jsonDslArray {
        }.build()

        assertThat(json).isEqualTo("[]")
    }

    @Test
    fun `add empty object to json`() {
        val json = jsonDslObject {
        }.build()

        assertThat(json).isEqualTo("{}")
    }

    @Test
    fun `add array with one element`() {
        val json = jsonDslArray {
            add("Hallo JSON")
        }.build()

        assertThat(json).isEqualTo(""" ["Hallo JSON"] """.trim())
    }

    @Test
    fun `add array with two elements`() {
        val json = jsonDslArray {
            add("Hallo")
            add("JSON")
        }.build()

        assertThat(json).isEqualTo(""" ["Hallo","JSON"] """.trim())
    }
    @Test
    fun `add array with two elements and convenience`() {
        val json = jsonDslArray {
            + "Hallo"
            + "JSON"
        }.build()

        assertThat(json).isEqualTo(""" ["Hallo","JSON"] """.trim())
    }

    @Test
    fun `add attribute to json object`() {
        val json = jsonDslObject {
            put("name", "JSONDSL")
        }.build()

        assertThat(json).isEqualTo(""" {"name":"JSONDSL"} """.trim())
    }

    @Test
    fun `set attribute to json object`() {
        val json = jsonDslObject {
            "name" set "JSONDSL"
        }.build()

        assertThat(json).isEqualTo(""" {"name":"JSONDSL"} """.trim())
    }

    @Test
    fun `create an array with nested array`() {
        val json = jsonDslArray {
            array {
                + "Nested"
            }
        }.build()

        assertThat(json).isEqualTo(""" [["Nested"]]   """.trim())
    }

    @Test
    fun `create an array with nested object`() {
        val json = jsonDslArray {
            obj {
                "name" set "Test"
            }
        }.build()

        assertThat(json).isEqualTo(""" [{"name":"Test"}]   """.trim())
    }

    @Test
    fun `create an object with nested array`() {
        val json = jsonDslObject {
            "name" array {
                + "Nested"
            }
        }.build()

        assertThat(json).isEqualTo(""" {"name":["Nested"]}   """.trim())
    }
}