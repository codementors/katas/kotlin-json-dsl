package com.example.openapidsl

import com.fasterxml.jackson.databind.ObjectMapper
import java.util.*
import kotlin.collections.HashMap

@DslMarker
annotation class JSONDslMarker

interface JSONDslBuilder {
    fun build(): String
}
@JSONDslMarker
interface JSONDslArray {
    @JSONDslMarker
    fun add(value: Any?)

    @JSONDslMarker
    fun array(block: JSONDslArray.() -> Unit)

    @JSONDslMarker
    fun obj(block: JSONDslObject.() -> Unit)

    @JSONDslMarker
    operator fun String.unaryPlus() {
        add(this)
    }
}

@JSONDslMarker
interface JSONDslObject {
    @JSONDslMarker
    fun put(name: String, value: Any?)

    @JSONDslMarker
    infix fun String.array(block: JSONDslArray.() -> Unit)

    @JSONDslMarker
    infix fun String.set(value: Any?) {
        put(this, value)
    }
}

fun jsonDslArray(block: JSONDslArray.() -> Unit): JSONDslBuilder = JSONDslArrayImpl().apply(block)
fun jsonDslObject(block: JSONDslObject.() -> Unit): JSONDslBuilder = JSONDslObjectImpl().apply(block)


class JSONDslArrayImpl : JSONDslArray, JSONDslBuilder {
    val elements = LinkedList<Any?>()

    override fun add(value: Any?) {
        elements.add(value)
    }

    override fun array(block: JSONDslArray.() -> Unit) {
        add(JSONDslArrayImpl().apply(block).elements)
    }

    override fun obj(block: JSONDslObject.() -> Unit) {
        add(JSONDslObjectImpl().apply(block).elements)
    }

    override fun build(): String {
        return ObjectMapper().writeValueAsString(elements)
    }
}

class JSONDslObjectImpl: JSONDslObject, JSONDslBuilder {
    val elements = HashMap<String, Any?>()

    override fun put(name: String, value: Any?) {
        elements.put(name, value)
    }

    override fun String.array(block: JSONDslArray.() -> Unit) {
        put(this, JSONDslArrayImpl().apply(block).elements)
    }

    override fun build(): String {
        return ObjectMapper().writeValueAsString(elements)
    }
}
